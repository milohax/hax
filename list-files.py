import os

from datetime import datetime
print(datetime.now())

# Specify the directory path
directory = '.'  # '.' represents the current directory

# List all files in the specified directory
files = os.listdir(directory)
print(datetime.now())

# Print each file name
for file in files:
    print(file)
print(datetime.now())
