# Here is Mike's Lab

In this Project I am keeping personal [hacking logs and experiments](https://gitlab.com/milohax/hax/-/issues?label_name%5B%5D=log&state=all), [experiment notes](https://gitlab.com/milohax/hax/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=log%3A%3Aexperiment), [code](https://gitlab.com/milohax/hax/-/tree/master), [reference notes](https://gitlab.com/milohax/hax/-/wikis/pages), [Issues](https://gitlab.com/milohax/hax/-/boards), and [Snippets](https://gitlab.com/milohax/lab/snippets) which are not part of any *specific* Project. It's an ***open labbook*** which anyone may refer to, create Issues in, Clone/Fork or update with a Merge Request.

 * [Issues](https://gitlab.com/milohax/hax/-/boards) track tasks that have no other Project home, and for [Field Notes](https://gitlab.com/milohax/hax/-/wikis/Fieldnotes) on Support Tickets
 * The [Wiki](https://gitlab.com/milohax/hax/-/wikis/home) is for keeping simple [reference notes](https://gitlab.com/milohax/hax/-/wikis/pages), and meta-notes about this Project
 * The [Repository](https://gitlab.com/milohax/hax/-/tree/master) keeps code for tools and experiments, I may also merge into other Groups' Projects
 * [Snippets](https://gitlab.com/milohax/hax/snippets) should be generally applicable to my roles at work or hacking in general.

My lab book Project is also a meta-project, of sorts, where I'm documenting how to use Projects within GitLab, for personal use. It suplements [my blog](https://milosophical.me/blog) as well as specific projects.

Feel free to poke around, if you see something that's wrong, could be improved, or that you think belongs in a shared place, you are very welcome to @-mention me, or make an Issue or an MR.

The idea for this Project was first explored in [my GitLab labbook](https://gitlab.com/mlockhart/lab). That's also worth checking out, even if you don't work at GitLab.

Happy Hacking!
